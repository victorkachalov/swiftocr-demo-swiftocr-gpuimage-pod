//
//  ViewController.swift
//  SwiftOCR Demo
//
//  Created by iOS_Razrab on 28/09/2017.
//  Copyright © 2017 iOS_Razrab. All rights reserved.
//

import UIKit
import SwiftOCR

class ViewController: UIViewController {

    @IBOutlet weak var myImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swiftOCRInstance   = SwiftOCR()
        
        swiftOCRInstance.recognize(myImage.image!) {recognizedString in
            print(recognizedString)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

